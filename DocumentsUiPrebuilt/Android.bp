//
// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package {
    default_applicable_licenses: [
        "Android-Apache-2.0",
        "vendor_unbundled_google_modules_DocumentsUiPrebuilt_license",
    ],
}

license {
    name: "vendor_unbundled_google_modules_DocumentsUiPrebuilt_license",
    license_kinds: [
        "SPDX-license-identifier-Apache-2.0",
        "SPDX-license-identifier-BSD",
        "SPDX-license-identifier-GPL",
        "SPDX-license-identifier-GPL-2.0",
        "SPDX-license-identifier-MIT",
        "SPDX-license-identifier-Unicode-DFS",
        "SPDX-license-identifier-W3C",
        "legacy_unencumbered",
        "legacy_by_exception_only",  // by exception only
    ],
    license_text: ["LICENSE"],
}

soong_config_module_type_import {
    from: "packages/modules/common/Android.bp",
    module_types: ["module_android_app_set"],
}

module_android_app_set {
    name: "DocumentsUIGoogle",
    set: "DocumentsUIGoogle.apks",
    privileged: true,
    prefer: true,
    soong_config_variables: {
        module_build_from_source: {
            prefer: false,
        },
    },
    overrides: ["DocumentsUI",],
    required: ["GoogleDocumentsUI_permissions.xml",],
}

prebuilt_etc {
    name: "GoogleDocumentsUI_permissions.xml",
    src: "GoogleDocumentsUI_permissions.xml",
    sub_dir: "permissions",
}
